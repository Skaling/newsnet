from newsnet.app.Model import base


class User(base.Base):

    def all(self, query="MATCH (p:User) return p"):
        with self.driver.session() as session:
            results = session.read_transaction(self._make_query, query)
            list_of_users = [{"id": node.id, "labels": list(node.labels), "properties": dict(node.items())} for node in
                             list(results.nodes)]
            self._close_connection()
            return list_of_users

    def search_user(self, query="MATCH (p:User) WHERE ", **kwargs):
        with self.driver.sessio() as session:
            self._buildSearchQuery(query, **kwargs)
            results = session.read_transaction()

    class Meta:
        name= "Person"
