import os
from neo4j import GraphDatabase

class Base:

    def __init__(self):
        self.host = os.getenv('NEO4J_HOST', 'neo4j://localhost:7687')
        self.port = os.getenv('NEO4J_PORT', '7687')
        self.username = os.getenv('NEO4J_USERNAME', 'neo4j')
        self.password = os.getenv('NEO4J_PASSWORD', 's3cr3t')
        self._connect()

    def _connect(self):
        try:
            self.driver = GraphDatabase.driver(self.host, auth=(self.username, self.password))
        except Exception as e:
            print("Connection error: ", e)
            return e

    def _close_connection(self):
        self.driver.close()

    def _read_transaction(self, query=None, *args, **kwargs):
        with self.driver.session() as session:
            results = session.read_transaction(self._make_query, query=query, *args, **kwargs)
            return results

    def _make_query(self, tx, query, *args, **kwargs):
        result = tx.run(query, parameters=kwargs)
        result = result.graph()
        return result

    def filter(self, limit=None, return_param=None, **kwargs):

        base_query = f"MATCH (o:{self.__class__.__name__}) WHERE "
        param_query = " AND ".join([f"o.{param}=${param}" for param in kwargs.keys()])
        return_query = f" RETURN {' o.'.join(['o.', *[return_param]]) if return_param else 'o'}"

        complete_query = base_query + param_query + return_query
        results = self._read_transaction(query=complete_query, **kwargs)

        users = [{"id": node.id, "labels": list(node.labels), "properties": dict(node.items())} for node in
                         list(results.nodes)]

        return users

