resource "aws_api_gateway_rest_api" "newsnet_api_gateway" {
  name = "Newsnet API Gateway"
  description = "Central API for users, tags, posts, comments"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}