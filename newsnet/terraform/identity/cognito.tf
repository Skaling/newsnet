
resource "aws_cognito_user_pool" "newsnet_cognito_user_pool" {

  name = "newsnet_user_pool"
  auto_verified_attributes = ["email"]

  password_policy {
    minimum_length = 6
    require_numbers = false
    require_symbols = false
    require_uppercase = false
  }

  tags = {
    Project = "newsnet"
  }
  
  schema {
    name = "email"
    attribute_data_type = "String"
  }
  schema {
    name = "nickname"
    attribute_data_type = "String"
  }
}

resource "aws_cognito_user_pool_client" "newsnet_cognito_client_app" {

  name = "newsnet_client"
  user_pool_id = aws_cognito_user_pool.newsnet_cognito_user_pool.id
  explicit_auth_flows = ["ALLOW_REFRESH_TOKEN_AUTH","ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_USER_PASSWORD_AUTH", "ALLOW_CUSTOM_AUTH"]
  generate_secret = false
}

resource "aws_cognito_user_pool_domain" "newsnet_domain" {

  domain = "newsnet"
  user_pool_id = aws_cognito_user_pool.newsnet_cognito_user_pool.id
}

