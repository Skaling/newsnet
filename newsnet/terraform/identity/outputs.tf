output "app_client_id" {
  value = aws_cognito_user_pool_client.newsnet_cognito_client_app.id
  description = "App client ID of user identity"
}

